# Streets of Rage 4 - Blaze Fielding strategies

This is a **Streets of Rage 4** strategy guide for **Blaze Fielding**.

![Blaze Intro](./assets/images/Splash_Intro.png)

## Authors note

This guide is intended to supplement full-combo videos I have published to [this](https://www.youtube.com/playlist?list=PLki1pvbw2f7DKpNy6juL_2tLjR9wixxXJ) YouTube playlist. If you somehow ended up here from a YouTube link, or any other means, I hope the information here does bring you some value.

My ultimate wish is that one day, what is shared here might help a God tier gamer complete the **Arcade Mania All Stages Full-Combo** challenge, which I won't be attempting myself.

PS: I am by no means a writer, and I've chosen to maintain this documentation purely as a passion project. So please, pardon any crappy writing you might come across.

-*FuzzYetDeadly*

## Overview

This is a work in progress, and will be gradually updated as fast as the videos to complement each topic can be produced. This process can take anywhere between weeks to months, depending on how challenging the levels are to complete a full-combo (because I can't accept anything less than that lol).

This guide begins by introducing terms to some of the **techniques** used across the various stages. It then dives into the tips and tricks for individual *stages*; with each page detailing the general strategies for various *sections* of each stage. As an extra, an breakdown of the theoretical maximum score possible for each stage is also available.

## Contents

* [Techniques](./extras/techniques.md)
* [Tips & tricks](./guides/tipsandtricks.md)

## Extras

* [SOR4 swapper mods](./extras/swappermods.md)
* [Game version switching](./extras/gameversionswitching.md)

## Misc. notes

**Streets of Rage 4 (SOR4)** is a *beat-em-up* game developed by **Dotemu** and **LizardCube**, and a faithful sequel to the Streets of Rage franchise which hasn't seen any love since more than 20 years ago (from 2020). The reason I love this game so much, is that I grew up with the *beat-em-up* genre. Thus, SOR4 has allowed me to relive some of the best memories of my childhood.

If you're having trouble finding partners for co-op play, you could consider looking for partners at official discord at: https://discord.gg/sor4
