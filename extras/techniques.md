# Techniques

[*Return* ![back](../assets/icons/icon_home.png)](../README.md)

The **tips & tricks** topics to follow will make references to some of the following terms. This section doe not cover the new moves in the DLC, as the combos were all completed using the vanilla move-set.

Please do take note of them, as to avoid any potential confusion to your reading experience.

## Air cancel

* Jump + Down attack on enemy wake  
This can cancel the wake attack/evasion of most enemies, and is critical for some stages to avoid chaotic situations
* Forward air cancel is a jump forward and down attack instead.

## Air cycle

* An air special that goes forward then backwards
* The second air special can be delayed a short while to extend air invincibility time

On some occasions, I refer to this with "slow":

* *Slow*: Forward jump to full height before the air specials

## Air dash

* Not to be confused with the term *air special*  
This is a forward jump to moderate height, then air special

On some occasions, I refer to this with "fast" or "slow":

* *Fast*: Upwards jump, and immediately air special
* *Slow:* Forward jump to full height before the air special.

## Air ride

* Bread and butter tech to extend combos across long distances
* Usually used as a follow up to attacks that "high bounce" opponents
* Can be opened with jump kick, forward jump kick, or jump down attack
* Can be done up to 3 times before the enemy lands, but the third is quite risky

## Flurry

* Not to be mistaken for McDonald's McFlurry
* This refers to the basic combo string, usually referenced with the number of hits to use

## Grab forward combo

* Hold forward on your d-pad while facing a grabbed opponent. Hit them up to 3 times.

## Jump kick

* Not to be confused with forward jump kick  
This is the one where you don't press the d-pad and jump straight upward and kick.

## Reverse neutral special

* Usually used as a follow up to charge attacks against a wall
* The trick to it is to press the back and up diagonal and special immediately after the charge hit connects

## Tactical grabbing

* When you grab enemies, holding them will prevent the combo counter from expiring
* This is a very useful trick to hold the combo hostage while waiting for enemies that like to "afk" to come at you
* This won't work on enemies that can break grabs quickly (obviously)

## Wall combo (#attack, ...)

* Where *#attack* is a placeholder for the attack to use, with a limit of 3.
* Attacks include; Forward special towards wall (S) and Charge attack (C).

For example:

```
Wall combo (SCS)
```

Would mean Forward special, Charge attack, Forward special
