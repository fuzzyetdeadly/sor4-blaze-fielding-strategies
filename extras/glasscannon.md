# Glass cannon

[*Return to mods index*](./swappermods.md)

This is intended as a custom difficulty in two flavors based on **Mania** and **Mania+**.

It is, as the title suggests, a way to play stages or arcade in the game as if you had the "Glass cannon" survival perk. However, this is only possible by halving enemy health, as a way to mod character damage doesn't yet exist.

No characters/items/stages are swapped, to preserve the original experience created by the game as much as possible.

![Glass cannon](../assets/images/Mods_GlassCannon.png)

## Available versions

| Versions | Description |  
| --- | --- |
| [**Glass Cannon+ v1.2**](../assets/modfiles/FuzzYDs_GlassCannon+_v1.2.swap) | Balanced for Mania+ |
| [**Glass Cannon v1.2**](../assets/modfiles/FuzzYDs_GlassCannon_v1.2.swap) | Balanced for Mania (requires special loading steps) |
| [**Archived**](../assets/modfiles) | Archived versions |

![Mind:](../assets/icons/icon_info.png) It is recommended to play the latest version for a better balanced experience. Archived versions have the following problems:

* Too hard to get extra lives due to enemy health being too low
* Bosses being too easy to beat (they can't even enrage)
* AI was too slippery in Mania+ difficulty, making it hard to get extra lives

## Features

* It will be harder, but not impossible to accumulate extra lives
* There are more enemies, and they move faster to make up for their lack of health
* Enemies will AFK less and be more aggressive
* Player defense is halved. This does not mean you'll get killed in one hit, but there are situations where it can happen.
* Due to the change in enemy spawns, there are certain areas of the game where the spawn pattern has changed. This can make for some unexpected scenarios

## Notes

* This mod makes the game harder in terms of speed and tolerance to errors as opposed to inflating enemy health (which I'm really not a fan of as it drags out the game)
* It takes just over an hour to play it from start to end in arcade mode
* It's quite difficult to get S-ranks in this difficulty, but it's hypothetically possible. You'd need to hold together really long combos though

## Known issues

* When loading the Mania variant, the Swapper (as at 4.2.4), does not load the correct "based on" difficulty. It was intended for **Mania** to be the base difficulty, and **NOT Mania+**. It is recommended to manually correct this (including other customized numbers), if you wish to run the mod on Mania difficulty.

## Community runs

Table of people in the SOR4 community that have attempted/completed a run!

![Note:](../assets/icons/icon_info.png) a '+' after a version indicates a completion of Glass Cannon+

| Player(s) | Version completed | Character |
| --- | --- | --- |
| ITR Gaming, FinalCrashSor3, ASB, Tom | v1.2+ | [Shiva4 x2, Axel4, Skate3](https://www.youtube.com/watch?v=l09skYrgYQY) |
| FuzzYD, AM| v1.2+ | Blaze x2 *(link not available yet)* |
| Multi | v1.1+ | [Cherry](https://www.youtube.com/watch?v=K6rP9nEI_tQ) |
| FuzzYD | v1.1 - (sample) | [Blaze](https://www.youtube.com/watch?v=_Pa8Id4JoDM) |
| Multi | v1.0 - tougher | [Cherry](https://youtu.be/0-kG9w1Rb_4) |
| Anthopants | v1.0 - tougher (attempted) | [Floyd](https://www.youtube.com/watch?v=pVmSLETYZEI), [Skate3](https://www.youtube.com/watch?v=eG9zsZB4txQ), [Max4](https://www.youtube.com/watch?v=5roj2AIkqRQ) |

## Changelog

v1.2:

* Added tuned swap file for Mania+
* Re-balanced enemy volume to 135% from 150%, as the latter was causing some spawns to go crazy, which made some sections rather unplayable

v1.1:

* Tuned player defense to 50% from 25%, to give the player a better chance of not getting 1-shot killed
* Tuned enemy health to 50%, to make it easier to get extra lives (needed for later stages)

v1.0:

* Initial release
