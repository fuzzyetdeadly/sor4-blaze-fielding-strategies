# Switching between game versions

[*Return* ![back](../assets/icons/icon_home.png)](../README.md)

The release of the DLC brought upon us v07 of the game. While this introduced many new features, it has also taken away some things that were previously possible in v05. This quite heavily impacted Blaze, but maybe also some other characters.

Thus, I was motivated to find a way to be able to switch between v05 and v07, should I ever wish to do runs in v05 where I wasn't limited by the "nerfs" that were made. However, I also saw value in sharing this with the speed-running community, as I figure there may still be individuals that wish to run the older version of the game. This page will detail everything you need to know to be able to switch between game versions without too much of a hassle.

## Assumption

This page assumes you're running on **Windows**. I don't have a Mac or Linux, so I'm unable to verify the steps for them, but they should be pretty much the same.

## Before you begin

You will need:

* [Windows .NET (LATEST LTS VERSION)](https://dotnet.microsoft.com/download/dotnet) for the command line  
Get the latest **LTS** version that suits your operating system (x64 for 64-bit, x86 otherwise)
* [SteamRE Depot Downloader (LATEST VERSION)](https://github.com/SteamRE/DepotDownloader/releases), a third party tool for downloading older versions of games  
Just download the latest available version in it's ***.zip** form.

![Mind:](./../assets/icons/icon_info.png) Please make sure to back-up your **save.bin** file. It should be in your `$Documents\Streets of Rage 4 Save and Config` directory for Windows. Saves made by v05 will make you lose **ALL** your v07 progress (Survival, Unlocked moves, Mania+ scores, etc). So you'll want to be able to restore your save file when you switch back.

## How to set up version switching with v05

1. Install **Windows .NET (LATEST LTS VERSION)**  
2. Extract contents of **SteamRE Depot Downloader (LATEST VERSION)** to a path of your choice  
3. Go into the extracted folder and select the file path, then type `cmd`. This should open up the command line in the folder with the Depot Downloader (depot directory not reflected in image).  
![cmd_image](./../assets/images/cmd_image.png)
4. Once in the command line, you only need to run the following command. This will of course, only work if you own the game.

![Mind:](./../assets/icons/icon_info.png) The depot tool requires your Steam credentials to do a SAML authentication with the Steam authentication service. This then provides the tool access to download the game files from Steam. In case this is a concern for anyone, this should be safe.

```PowerShell
dotnet DepotDownloader.dll -app 985890 -depot 985891 -manifest 4108968486236684687 -username <your_steam_username> -password <your_steam_password>
```

For example:

```PowerShell
dotnet DepotDownloader.dll -app 985890 -depot 985891 -manifest 4108968486236684687 -username jondough -password ********
```

![Mind:](./../assets/icons/icon_info.png) The `app`, `depot`, and `manifest` arguments are all values retrieved from [steamdb.info](https://steamdb.info/app/985890/depots/). If you are not running on Windows, you will need to go there and locate the correct depot and manifest for your operating system. Follow the link, and locate `Manifests` category. Then, look for a `Manifest` ID dated around **September 2020** (which should be v05 with it's hotfix). I've also summarized this information [below](#) for convenience, Windows version only.

5. Let the download finish, then go to the directory where **Steam** installed your Streets of Rage 4. It should be at `$Steam\steamapps\common\Streets of Rage 4\`.  
6. Rename the existing `data` and `x64` folders with a prefix of your choice (I used _v0X_), where X is the version number. This allows you to easily switch between versions simply by re-naming folders (and not having to copy/paste them all over the place which takes longer)

![Rename](./../assets/images/extra_rollback.png)

7. Lastly, make sure you run your Steam in offline mode before launching the game, to stop it from patching back to v07.

That's basically it. If you want to roll-back to v07, you only need to re-name the folders. Thankfully it wasn't too complicated to set this up. Hopefully this will benefit others and not only myself (as I do have some unfinished business with v05).

## Manifest versions for Windows version

| Version | ManifestID | Description
|---| --- | --- |
| v07 | 4504550668476263526 | Latest stable v07 |
| v05 | 4108968486236684687 | Latest stable v05 (probably) |
| v04 (launch) | 2740642024377132416 | Launch version, may not actually be v04 |

The above are probably the versions of interest. There are more, but I can't remember what might have been fixed in each of them.
