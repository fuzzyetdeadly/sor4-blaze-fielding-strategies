# SOR4 swapper mods

[*Return* ![back](../assets/icons/icon_home.png)](../README.md)

A swapper for Streets of Rage 4 was created by **Hongangqi**, which allows you to customize quite a lot of things in the game by swapping things around. Custom difficulty settings were also made possible with this.

You can find the swapper [here](https://sourceforge.net/projects/sor4-character-swapper/).

The following mods were created for the swapper tool. They may not be the final versions, as I may decide to rebalance them in future :)

## Mods

* [Glass Cannon](./glasscannon.md)
* [The Fatpocalypse](./fatpocalypse.md)
