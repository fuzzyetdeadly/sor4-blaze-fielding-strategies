# Stage 10: To The Concert

[Return to index](./tipsandtricks.md)

[![Blaze Stage 10](./../assets/images/Splash_S10_Score.png)](https://www.youtube.com/watch?v=FD8bhVgjF_U)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

The strategies described for this stage may not match the video exactly, as I have made minor tweaks since the time of recording. The Grenade and Koobo fights are the hardest parts to get right, with the latter needing a bit of luck. Overall, this is one of the stages where its really easy to make mistakes if you lose focus.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Good)](#1-start)
2. [Kickboxers (Average)](#2-kickboxers)
3. [Grenade fight (Average)](#3-grenade-fight)
4. [Galsias and Bens (Good)](#4-galsias-and-bens)
5. [Wrecking ball (Average)](#5-wrecking-ball)
6. [Koobos fight (Average)](#6-koobos-fight)
7. [Kill the DJ (Good)](#7-kill-the-dj)

## 1. Start

### 1.1 Opening

* Jump forward, then move down a bit, try to maintain this position to avoid triggering **Gudden**

### 1.2 Strategy

* Wait for **Garam** to approach, then try a 4-hit flurry into defensive special
* If you're lucky, you'll hook both **Garams** behind you, and slam the **BTs** at the same time
* Forward special into jump kick twice to kill them all
* Jump forward, then jump forward and kick **Gudden**, hold charge attack
* Release the charge and immediately forward special, jump kick to kill the **Gudden** that enters
* 3-hit flurry, blitz and charge to kill the original **Gudden**
* [Optional] Break the bottom barrel, take the apple, then walk left and up a bit
* Kick the Josephs from as far as you're able, then wait for the **Guddens** to align
* Wall combo (SCS), jump kick, and air cycle (with a slight delay backwards), this should kill all **Guddens** and **Josephs**
* Jump left and kick right to get the **Jonathans** away from the weapons, then kill them
* [Optional] Take all the loot, then jump to the next section

### 1.3 Freestyle

General:

* If you mess up the wall combo, you'll likely need to rely on forward and defensive specials to clear this section

**Garam and BTs**:

* These guys don't always behave. So you might just need to punch them and defensive special when they come from behind. Follow up with a charge attack if timing allows.

## 2. Kickboxers

### 2.1 Opening

* Jump forward and walk diagonally right and down to position at the '+' on the ground

### 2.2 Strategy

* Wall combo (SC) the incoming kickboxers. Grab **Condor** as he wakes up
* Vault over **Condor**, then blitz and charge attack (buys a little time)
* The next few **Condors** requires some freestyle

### 2.3 Freestyle

Condors:

* The objective is to bait the Condors towards you, then punish them
* If they try to distance themselves, they're trying to jump. Step upwards to dodge, then grab and punish
* If they walk towards you, they want to kick you, just out of range them with your punch
* It seems only one of them can aggro at a time, so the trick to this part is to keep an eye on the one that is currently aggro-ing. If he gets too close, defensive special
* They can take awhile to approach, you can grab one of them to hold the combo if this happens
* When there's only 2 left you need to try and cross the rooftop to avoid dropping the combo. I usually do this with blitz into air specials (seems safer)

## 3. Grenade fight

### 3.1 Opening

* This part is quite tricky. After breaking the tin with the apple, position your front foot on the apple, then air dash

### 3.2 Strategy

* Inch forward a bit if you need to, 3-hit flurry, charge into forward special, then jump kick, this should kill Z if you dashed in from the right distance
* **Honey** and **Sugar** should be right in front of you now. There should be enough time to 3-hit flurry them twice, then blitz twice before **Caramel** can reach you. This should kill both of them, and the **Margaret** that jumps down
* Follow up with a jump double down attack, and air cycle. This will either hit **Caramel**, or she will now be at your right. Regardless of circumstances, you should now try to kill the second **Margaret** while avoiding her. This should be a lot easier with only 2 of them to deal with

### 3.3 Freestyle

* Sometimes you might get unlucky and **Z** survives, or **Sugar** and **Honey** can't be hit at the same time. It's a bit hard to recover when this occurs, but you'll likely have to open with a forward special, then move right and blitz, neutral jump kick, and air cycle to recover
* You generally want to kill the **Margarets** first, while jabbing **Caramel** to hold the combo. They're vulnerable while doing their throw animation, and will jump around otherwise, resulting in the combo banking
* To deal with **Caramel**, just wait for her to charge, then time an air cancel when she's incoming, and jab lock her to death, or use her to get into a better position

## 4. Galsias and Bens

### 4.1 Opening

* N/A

### 4.2 Strategy

* Approach the **Garams**, then 3-hit flurry, blitz, forward jump kick, air special to kill them
* Move diagonally down and right a bit, then jump right and kick the **Josephs** left
* Forward special the enemies coming at your with knives, then jump forward and deal with them however you like
* Wait for Anry to enter, then 3-hit flurry him twice, then grab and forward hit him twice
* Wait for the Bens to enter and start charging, then blitz into air cycle. All Bens should now be against the wall
* Time 4 forward specials to hit when **Anry** is waking up. If done right it should keep the Bens pinned while killing **Anry**
* Finish the **Bens** off with an air dash (easier to get off the rooftop)

### 4.3 Freestyle

**Bens**:

* Sometimes they'll get out of sync. You either need to spam forward special or use the knives at your feet to deal with that
* If **Anry** gets free, don't let him cross you on the X-axis, or he will roll. Follow him diagonally left and down, then punish him when he's close enough. If the **Bens** get too close, just forward special

## 5. Wrecking ball

### 5.1 Opening

* This part is also a bit tricky. When you're throwing enemies, keep an eye on the position of the wrecking ball. You generally want to time throws when the ball is close to you (to avoid backswing that might hit you)
* Move diagonally upwards, then time a forward special after Gudden hits the wrecking ball

### 5.2 Strategy

* Jab the entering Jonathan, then grab him and throw him backwards
* Jab the entering Garam, then grab him and throw him backwards. If the ball is too far to the right, you'll need to vault and forward special instead
* From here on some freestyle is needed

### 5.3 Freestyle

* This part is a lot easier when you remember when the "**Galsias**" are entering
* Use jump kicks to keep the ball swinging
* For waves of "**Donavans**", forward special to prevent them from getting knocked to the left
* If one "**Galsia**" enters, jab and throw him backwards while watching the balls location
* If more than one "**Galsia**" enters at the same time, you'll likely have to forward special, then jump left and kick right to stay safe. Punish them however you like and get back in position to kick the ball
* [Optional] Leech your life back by doing double jabs on the wrecking ball, then grab all the loot

## 6. Koobos fight

### 6.1 Opening

* Approach the second ball, jab it twice, then forward special. Back off and position where you can just kick the ball as it reaches the left. Kick it until all "**Donavans**" are dead
* The consistency of the next part depends a bit on luck

### 6.2 Strategy

* Keep timing kicks on the ball to hold the combo. **Koobos** will enter
* If you're lucky, you should be able to kick the ball into the **Koobos** 3 times before the "**Donavans**" enter
* Wait for the "**Donavans**" to get in sync, then defensive special, forward special left, defensive special, and forward special right. If you're lucky, the Koobos didn't interrupt, and the "Donavans" should be dead
* Finish the **Koobos** off however convenient, but watch out for flying projectiles
* Deal with any "**Galsias**" that have entered however you like, then take the goodies and move on

### 6.3 Freestyle

* If the **Koobos** start their telekinesis attack or begin circling while you're dealing with the "**Donavans**" its likely you'll need to use a star here to avoid getting hit. The design of the arena makes it a bit hard to see projectiles that are about to hit you
* If you take too long to kill them, extra **"Donovans:** will enter, so you generally want to finish this quick

## 7. Kill the DJ

### 7.1 Opening

* [Optional] Move diagonally left and up, back attack the boxes and take the loot. Do a slow air dash to hold the combo while closing the gap to the **DJ**
* I prefer to kill the DJ before he can enter his second attack phase here, or the fight gets quite annoying. The combo string must be exactly like below to ensure his death
* To "touch of death" the **DJ**, start with two 3-hit flurries, grab and hit him twice, then blitz
* Follow up with a jump kick, air special, charge attack and reverse neutral special
* Then a double down attack, and air cycle. Immediately face right (this baits any **Signals** towards you), and use a star. The best case scenario is that you catch all enemies in your star, or it only misses the **Bodyguard** (who also misses his shot)
* Wall combo (SC) the DJ, then jump kick, air special, charge attack, reverse neutral special once more
* Finish with either a jump double down attack, or another neutral special (if there are too enemies close by). That's it

### 7.2 Strategy

* Break the **DJs** shield, using defensive special as necessary to avoid hits. Aim to be standing in front of him, and not behind him when the shield breaks (or the instant kill combo won't do enough damage)

### 7.3 Freestyle

* The **DJs** weak spot is his back. Most of his attacks won't hit you if you're behind him so he is a bit of a joke
* Unfortunately, I don't have a good way to freestyle him if he doesn't die in the first combo, because the interrupts from his minions are really annoying. So, should you need to deal with his minions, kill the **Bodyguards**, then **Signals** before attacking him, or they are going to keep interrupting your combos. Good luck

## Theoretical maximum score

Estimated: **138k+**

Lost points:

* Pickups: 300
* Combo bonus: 10k+ (from damage of additional mobs)
* Health: 270
* Stars: 1.5k
* Time: Variable
