# Stage 0X: Template

[Return to index](./tipsandtricks.md)

[![Blaze Stage X](./../assets/images/Splash_S0X_Score.png)](https://www.youtube.com/watch?v=RnrWSmTTQXw)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

Lorem Ipsum

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Good)](#1-start)

## 1. Start

### 1.1 Opening

* Lorem Ipsum

### 1.2 Strategy

* Lorem Ipsum

### 1.3 Freestyle

* Lorem Ipsum

## Theoretical maximum score

Estimated: **XXk+**

Lost points:

* Pickups: Xk
* Combo bonus: Xk+ (from lost damage)
* Health: XXX
* Stars: Xk
* Time: Variable
