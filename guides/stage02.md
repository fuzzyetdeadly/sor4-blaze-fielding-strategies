# Stage 02: Police precinct

[Return to index](./tipsandtricks.md)

[![Blaze Stage 2](./../assets/images/Splash_S02_Score.png)](https://www.youtube.com/watch?v=7xdVjLwsPlg)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

One of the more consistent stages once you get used to it. The hardest part is probably the retro stage extension as its quite error prone. Boss fight can be a little trick as well if you want to farm points with the cops.

Since the time of recording, I found some better ways to tackle some parts, so this guide may not perfectly reflect what's in the video.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Average)](#1-start)
2. [Breaking out (Good)](#2-breaking-out)
3. [Barney and Co. (Average)](#3-barney-and-co)
4. [Jack (Good)](#4-retro-stage)
5. [Security room (Good)](#5-security-room)
6. [Office (Good)](#6-office)
7. [Commissioner (Average)](#7-boss)

## 1. Start

### 1.1 Opening

* Move down until you're in line with the **Donovans**
* Forward special into charge to kill them

### 1.2 Strategy

* This section requires freestyle
* You'll want to try and kill the last enemy near the right of the screen. It's easier to connect to the next area

### 1.3 Freestyle

* Depending on the situation, you'll need to defensive or forward special to stay safe
* Use what works best for you to wipe the enemies out
* Try to kill **BT** first, he is the highest risk because of his quick jab
* For **Signals** and **Dylans**, they like to move up and down. Tactical grabs can help you hold the combo. Waste them and continue on

## 2. Breaking out

### 2.1 Opening

* The jail cell can take 5 hits. Jump forward kick for the last hit
* [*Optional*] Move towards the water cooler, back attack and take the food points

### 2.2 Strategy

* Hold charge attack, release and immediately forward special on the **Ferrocios**
* Move or jump kick forward and repeat again. This should clear the first mob
* Forward special into charge to kill the next 2 entering cops, then immediately hold charge
* Release the charge and immediately forward special when the Galsias are aligned upon their jail cell exit
* Forward special into charge to kill the next 3 entering cops
* Repeat the same for the entering **Dicks**, finish them off with a jump kick
* Move forward, and start moving diagonally up when you're on the broken tiles
* Try to time a grab on *Ferrocio*, and suplex him on the entering G. Signal
* Forward special into charge and jump kick. If you're lucky 3 of them will be dead by now
* Wait for **Galsia** to break **Dick's** grab, then jump forward and kick backwards
* Jump in and double down attack *Dick*, into charge forward special. *Dick* should die
* Bait *Galsia* upwards, then jab (for safety), grab and throw him backwards
* You can also back attack charge *Galsia* if you're comfortable with the inputs

### 2.3 Freestyle

**Ferrocio**:

* Will sometimes circle behind you
* Can suddenly jab you if you walk into his range
* Jump down attack, grab, throw backwards to get them grouped again

## 3. Barney and Co.

### 3.1 Opening

* Approach Barney diagonally, and start punching him before his AI activates
* Flurry him 3x twice, charge, forward special, then jump kick and air special backwards
* *Alternatively*, Flurry him 3x, charge, forward special, double down attack, charge and defensive. This might put you in a better position to wipe the entering Dicks, but further experimentation is needed to draw a conclusion

### 3.2 Strategy

* Defensive into charge forward special to kill the entering *Galsias*
* Charge forward special once more, then jump kick and air special backwards for a safer positioning
* Some freestyle is required here.
* What sort of worked for me was to punch one side and defensive when the other got close, and follow up from there
* Blitz and forward special are a bit risky due to longer recovery time, so watch out for those
* Try to keep one *Dick* alive if you wish to extend to the retro or security room
* Assuming no retro, in v07, you need to back attack, jump forward kick, air special, land and blitz, jump down attack, and air special twice to reach the next area
* See [retro stage](#4-retro-stage) for instructions how to extend through it. This will only work if you don't break the water fountain with the pipe in it

### 3.3 Freestyle

* Using specials at the right time makes this section a lot easier
* Take the chicken in the water fountain if your green health is getting too big

**Dicks**:

* *Flashing red*: Count by jump kick or jump down attack when they get close, then punish
* Use forward special to push them away if there are too many
* You can also grab and throw them into each other if things get too messy
* Worst case scenario, jump kick left and right while avoiding Donovan to stay safe, using air special as necessary

**Donovan**:

* Can be a bit tricky to kill quickly with the cops around
* Use forward specials to play it safe
* Pushing them into the cops will make them fight, you may or may not want this (depending on whether you're optimizing your score)

## 4. Retro stage

### 4.1 Opening

* Make sure *Dick*'s life is low enough to kill him in one jump kick
* Grab the taser, then grab and release Dick to try and position him closer to the arcade machine
* Once in a good position, jump kick, then jump forward kick him
* Taser the machine

### 4.2 Strategy

* Move diagonally downwards, and jump forward kick to knock Jack down
* Get the chicken while your life is full
* Grab Jack, blitz him twice, then jump double down attack him 2-3x
* Get the star, then grab him, make sure you're facing right
* Blitz into charge (buys a bit more time), and exit the machine
* Immediately move diagonally up and left, break the water fountain and get the pipe
* Jump forward, then jump forward and throw the pipe
* Jump forward and grab the pipe, then take the money
* Move around the wall and throw the pipe again to extend to the next area

### 4.3 Freestyle

* N/A

## 5. Security room

### 5.1 Opening

* Grab the water fountain money bag to hold the combo

### 5.2 Strategy

* Try to time a forward special into charge, then defensive when **BT** gets close enough.
* Follow up with a charge forward special left. If you're lucky this will kill 3 *Dicks* and a **BT**
* Some freestyle required here
* You'll want to try and position so enemies are all at your left
* However, you also want to try and finish close to the right wall (to extend to **Murphy**)
* Break **Murphy**'s shield however convenient (see *freestyle*), kill him, take the *baton* and move on

### 5.3 Freestyle

* Use whatever works best for you to kill the remaining enemies
* Forward special into charge and charge into forward special are both great ways to do this
* Usually I'll start from the right wall and work my way left
* If you want points from the apple, you'll have to stick at the right wall and punch

**Murphy** shield break tactics:

* These are in order of ease of execution
* 3 hit flurry, defensive, charge
* Forward special from distance, hold charge, approach and release, then forward special
* Jump kick in, hold charge, land and release into reverse defensive special
* These will also be useful for the next section

## 6. Office

### 6.1 Opening

* This section is a bit long, so I had to split the strategy section up a little
* Throw the baton, and take the money in the vending machine [*Optional*]
* Hold charge and approach the *Ferrocio* and *Galsia* fight

### 6.2.1 Strategy (Start)

* Release charge and forward special to kill them
* Move to *Dick*, do a 3-hit flurry and blitz to kill both enemies
* If *Dick* dies before *BT*, more *Murphys* will appear. You may prefer more *Signals*, but I find *Murphies* less troublesome (they don't move around as much)
* Forward special into charge to kill the two *Murphys* entering from the center
* Some freestyle required from here
* Aim for the *Signals* first, they are a higher risk of combo drops
* Then try to finish off the *Murphys* while positioning near the water cooler

### 6.2.2 Strategy (Mid-office)

* Grab a baton, then take the food from the water cooler
* There are 3 Murphys at the next area. You'll want to throw the batons dropped by the Murphys you just killed at them (some aim required). This will disarm them and make the next section easier
* Go straight for Ferrocio and Galsia again, kill them however convenient
* Kill Kevin next with forward into charge attack so you don't have to fight on two fronts
* Kill all the remaining *Murphys* and *Dicks* however convenient, it should be quite trivial
* Personally, I just nuke them with charge into forward special loops until they're all dead
* Take the goodies and move on

### 6.2.3 Strategy (Barneys)

* Move towards the Barneys, and air dash them when they try to grab you
* They should be together against the desk now
* To kill them, just do blitz, jump double down, charge, reverse neutral special
* Then jump double down into charge, wait for them to get up
* Take a taser and jump kick to finish them, then go for the food in the cooler
* Turn around and throw the taser at *Galsia*, then position and jump forward kick and kill the other *Galsia*
* Bait the remaining *Galsia* below the desk, kill him there. This allows an easier extend to the entering Murphy
* Break *Murphy*'s shield, then turn around and kill *BT* with forward special into charge. If you don't do this fast he might circle around the desk
* Kill *Donovan*, then *Murphy* in the same way
* *Dick* and *Murphy* will enter, use a wall combo (SCS) to kill *Dick* and shield break *Murphy* at the same time
* Blitz *Murphy*, and try to finish him off with an air special to easily connect to the next section

### 6.2.4 Strategy (End-office)

* Go straight for the 2 Murphys blocking the door first (safest setup I could find). Shield break them with the wall combo (SCS), and kill them quick. The door will usually break at the same time.
* Deal with the remaining Murphys however convenient
* Take a baton and move on to the boss
* [*Optional*] If you don't want the extra goodies from killing Murphies, you can just go straight into the office.

### 6.3 Freestyle

**General**:

* If you get pincered, defensive special into backwards charge and forward special usually kills whoever is trying to sneak up behind you

**Signals**:

* You can bait Signals to you by facing your back to them, then jump down attack when they are near
* Its safer to fight them a bit to the left, where *Murphy* can't sneak up and hit you from behind

## 7. Boss

### 7.1 Opening

* Throw the baton, then catch it
* Move up a little, throw the baton at the water cooler, then move diagonally down and left
* Once Commissioners super armor expires, forward jump kick him, blitz, then take the chicken for points
* Rest of the fight can be easy or hard depending on whether you wish to farm points

### 7.2.1 Strategy (Easy)

* While positioned against the right wall, jab lock the commissioner to death
* When cops come to help, just time your jabs to make sure they get hit, that's all there is to it

### 7.2.2 Strategy (Hard)

* Hug the right wall until Commissioners armor expires, then try to damage him enough that his backup arrives
* Run for the apple and try to kill the 4 cops that enter quick
* To drag this fight out, you need to try keep the boss at the right, while killing cops entering from the left. This requires quite a bit of freestyle
* The best case scenario is that you kick the boss right, then kill a wave of entering cops, and repeat. This minimizes damage on the boss so he doesn't enrage too quickly
* Once you're satisfied with the amount of cops you've wiped, finish the fight

### 7.3 Freestyle

**General**:

* You need to see the situation and move around the room to avoid getting hit while holding the combo
* Jump kick and air dashing around seems to be useful
* Use your stars if you have to, it can make this a lot easier

**Cops**:

* Best way I could find to deal with them is forward special, (forward) jump kick, air special. It can kill them when they're entering, and also keep you safe from the boss
* For Barneys, I like to jump kick and air special around to stay safe while Commissioner is trying to do his left/right grabs. Once they're dead, the boss should be pretty easy

**Boss**:

* Try to avoid him but jump forward kicking him away
* *Flexing attack*: Best to just stay away from him. Can hold combo by using forward special on him
* *Grab left/right attack*: Try to stay in the air by kicking cops around to avoid it

## Theoretical maximum score

Estimated: **143k+**

Lost points:

* Combo bonus: 3.5k+ (from lost damage)
* Health: 750
* Stars: 1.5k
* Time: Variable
