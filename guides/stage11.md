# Stage 11: Airplane

[Return to index](./tipsandtricks.md)

[![Blaze Stage 11](./../assets/images/Splash_S11_Score.png)](https://www.youtube.com/watch?v=yXHIoJk9x8Y)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

I had a **tremendous** amount of difficulty figuring this stage out, but was able to find a concrete strategy to it. This is personally my favorite stage, as it is actually possible to out-play the RNG with good consistency without too much timing/positioning issues.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Elevator (Good)](#1-elevator)
2. [Airstrip I (Good)](#2-airstrip-i)
3. [Airstrip II (Good)](#3-airstrip-ii)
4. [Entry ramp (Good)](#4-entry-ramp)
5. [Cargo (Good)](#5-cargo)
6. [Lounge (Average)](#6-lounge)
7. [Boss (Average)](#7-mr-y)

## 1. Elevator

### 1.1 Opening

* Inch forward a bit so all the **Galsias** land to your left

### 1.2 Strategy

* Forward special into neutral special to hook the enemies to the right
* Time a forward special after **D. Signal** lands, this should knock most of them out
* Deal with the remaining **Galsias** and **R. Signal** any way you like
* Position to grab **Francis**, throw him backwards to knock-out the **Brandons**. You probably need to do this twice
* Position to forward special two **Victorias** off the elevator
* Forward special into charge to kill another two (or just forward jump kick the center one down)
* Position to forward special another 3 **Victorias** off the elevator
* Jump to the left and grab the **Victoria**, use her to get closer to the crates
* If the other one isn't dead, kick her off

### 1.3 Freestyle

* Use the crates or wall bounce corpses to keep the combo alive
* *Optional*: use the last enemy of each wave to leech your life back

## 2. Airstrip I

### 2.1 Opening

* Align with the yellow line at the top, then air dash

### 2.2 Strategy

* Forward jump, forward jump kick, 3-hit flurry, neutral special, charge to break the first **Dunphy**
* 3-hit flurry, charge, forward special to kill him
* Wait for the 3 **Annes** to throw their bottles, then wall combo (SCS) them into jump kick. They should die
* Deal with the remaining **Dunphys** with freestyle, while trying to get them to the right wall

### 2.3 Freestyle

**Dunphy**:

* Use his shield to recover your life after he is disarmed
* Can break his shield with charge, neutral special, charge
* Use forward jump kick to get them to the right wall (it deals little damage)

**Anne**:

* Not very good at aiming, seems to throw at a fixed range
* Try not to cross her on the Y-axis, so she doesn't jump far to the left behind you

## 3. Airstrip II

### 3.1 Opening

* Grab and blitz the last Dunphy from the previous section to hold the combo into this one

### 3.2 Strategy

* Begin by attacking the center file with 3-hit flurry, neutral special, forward special
* Immediately forward jump kick and air special twice to get to the right wall
* Star power to break the top two barrels. You may need to neutral special a few times for safety if there are bottles incoming
* Reposition around the horizontal center of the strip to stay safe from electric bottles
* Some freestyle is required to deal with the remaining enemies

### 3.3 Freestyle

General:

* I prefer not to use the bottle to open this section, as it messes up the "safety" timing for getting to the right wall
* There are a few ways to extend to the next area, these will be described in section *4.1*

**Barney**:

* Highest priority. Usually dies to the star but sometimes survives
* Kill him as fast as possible however convenient while watching your own back for enemies

**Anne**:

* Second highest priority. Once again, try not to cross her on the Y-axis to avoid her bunny hopping
* Kill her with wall combo (SCS) into jump kick, but watch out for enemies trying to sneak up on you

**Dunphy**:

* Break his shield with the baton or 3-hit flurry, neutral special, charge
* Use to leech back life and also chain the combo into the next section
* Can forward jump kick him to get him to the right wall
* Can grab and release to re-position him up or down

## 4. Entry ramp

### 4.1 Opening

Extensions (from easiest to hardest):

* From the top yellow line, grab **Dunphy** and blitz into double air special. Forward jump twice, then forward jump kick to open the fight
* From the top yellow line, grab **Dunphy** and blitz into a neutral jump double (or triple) air-ride into the back of the plane. This should get you in a pretty good starting position
* Get **Dunphy** to the bottom yellow line, and pick up the taser. Last hit him with a jump kick into the barrel, then a forward jump kick to the left after the explosion. Immediately throw the taser to freeze the combo by stunning Dunphy. Take the star and move on. This is a tech invented by **BadMoon** (credits to him)

### 4.2 Strategy

* Try to wall combo (as long as it's safe) the **Jonathans** to kill them
* This section is quite trivial, just deal with the enemies however you like

### 4.3 Freestyle

* You may sometimes need to spam specials to be safe here
* Try not to get shanked, and watch out for bullets
* If you can prevent enemies from falling off, you get bonus points
* Try to position at the right wall to easily chain to the next section

## 5. Cargo

### 5.1 Opening

* Break the grenade box, take it, and throw it at the **Sands**

### 5.2 Strategy

* Break the apple box, take it (now or later should be fine), then 2-hit flurry, neutral special jump kick, and double air special the Sand highest on the vertical axis. This should kill him, making the last Iron enter
* If you're lucky, all Irons will now be at the right wall. Forward special them and close the distance
* Kill the **Irons** so they can't circle and pose a threat, then freestyle the **Sands** while trying to position the last against the right wall
* Grab the last Sand, blitz into double air special to land right in front of the Irons. Kill them with 3-hit flurry twice, charge, blitz. Immediately hold charge
* Release the charge on the two Sands entering from the bottom, then neutral special and forward special to try and break them. Sometimes the others may circle behind, so you'll need to jump kick and air special to the left for safety
* Freestyle the remaining **Sands** again, and proceed to the next section

### 5.3 Freestyle

**Sand**:

* Break his shield with charge, neutral special, charge/forward special/jump kick, depending on the situation
* Not a big threat after disarmed
* Can use him (and his shield) to leech back your life

## 6. Lounge

### 6.1 Opening

* You can approach directly or go for pickups, it's up to you
* Use the chairs at the left to hold the combo if you want to take the suitcase
* Walk past the suitcase a bit to trigger the jumping girls, then walk back to take the cash before moving forward

### 6.2 Strategy

* Wait for the entering girls to be in sync
* Time a forward special, jump kick, air special to get them along the wall
* Immediately jump backwards, and inch away a bit (to prevent them from jumping behind you)
* Time another forward special, jump kick, air special twice to kill them and land in the next area
* Move to the bottom of the table and freestyle the incoming jumping girls. Be careful not to overextend so **Irons** don't spawn. Forward special can be used to hold the combo
* After all of them are dead, trigger the **Irons** and use them to reposition to the right wall without breaks
* *Optional*: kill the last iron with an air dash, and you'll be in a nice position in front of the whip girls
* Wall combo (SCS), jump kick, air special, charge, neutral special the whip girls. This can sometimes kill 2 or 3 of them. Deal with the rest with freestyle

### 6.3 Freestyle

General:

* The jumping and whip girl parts are the most inconsistent fights for this stage. If they get out of sync they're quite troublesome

**Saphyr, Pyrop**:

* Can neutral or forward special to stay safe from them
* If they run towards you fast, they usually want to slap you. Counter by just punching them
* If it's safe, you may be able to kill them with 3-hit flurry twice, charge, forward special

**Iron**:

* Can forward jump kick on him to reposition yourself and hold the combo
* Can grab and release to position up and down vertically

**Whip girls**:

* Can be air cancelled on their wake
* Can be used to recover green life before the boss fight
* Use neutral or forward special to avoid getting hit by them

## 7. Mr. Y

### 7.1 Opening

* Move diagonally right and up, and forward jump kick. This should make Mr. Y evade backwards

### 7.2 Strategy

* Open with 3-hit flurry twice, grab forward attack twice, vault, blitz, jump kick, air special twice. This should get him to the left wall
* Blitz, jump kick, air special twice, then back off, wait for his wake and forward jump kick in again to make him evade. If you time it right, you should be able to continue the combo here
* Start another 3-hit flurry twice, grab forward attack twice, blitz into a jump kick double air-ride (using air special twice for the second time)
* Blitz to OTG him, jump down attack 3 times, then air cycle and land. Inch away a bit
* Wait for him to almost wake, then star. He should activate phase 2 of the fight, but immediately get picked up by the star
* Wall combo (SSC), open a double air ride with jump kick, then charge and reverse neutral special
* Blitz to OTG him one more time, then jump down attack 3 times into double air special and he should die

### 7.3 Freestyle

General

* If you're not able to kill him in one shot with the strategy, this will usually need some luck to hold the combo together
* I lucked it out in the reference video, but have also been able to full-combo him maybe 1 in every 2 or 3 attempts at best while I was working on this

**Mr. Y**:

* To beat the RNG in this fight, you need to execute two separate combos in the first and second phases of the fight to kill him before he can start hopping around
* For the first combo, you don't want to damage him too much, or it will immediately trigger his second phase at an unfavorable timing
* In the first phase of the fight, he will usually try to shoot you immediately on his wake if you are a certain distance with your back facing him. This is the best opening I could find for him. Counter by forward jump kick, then open your combo. Mind that this is a quite timing sensitive, as he can evade multiple times in a row
* If you couldn't kill him in the second phase, you'll likely lose the combo. You can avoid damage from him by using air dashes around the room
* If you don't want to go through all the trouble above, then just infinite him

## Theoretical maximum score

Estimated: **146k+**

Lost points:

* Pickups: 4.2k* (excludes barrel goodies, except star)
* Combo bonus: 1k+ (from lost damage)
* Health: 140
* Stars: 1.5k
* Time: Variable
