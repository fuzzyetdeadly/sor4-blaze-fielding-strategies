# Stage 06: Chinatown

[Return to index](./tipsandtricks.md)

[![Blaze Stage 6](./../assets/images/Splash_S06_Score.png)](https://www.youtube.com/watch?v=xblgjCba8pU)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

Very difficult stage because it's long and there are many opportunities to make mistakes. Certain rooms also require a luck to pass safely as there are many bad patterns that can occur. It's only possible to play this as consistently as you're able to freestyle unfortunately.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Average)](#1-start)
2. [Before kitchen (Good)](#2-before-kitchen)
3. [Kitchen (Average)](#3-kitchen)
4. [After kitchen (Average)](#4-after-kitchen)
5. [Goros (Good)](#5-goros)
6. [Galsias (Good)](#6-galsias)
7. [Donavans (Good)](#7-donavans)
8. [Kickboxers (Bad)](#8-kickboxers)
9. [Boss (Average)](#9-random-number-god)

## 1. Start

### 1.1 Opening

* [Optional] Break the bikes and take the goodies
* [Optional] Break the bin and take the boomerang

### 1.2 Strategy

* Throw the boomerang and catch it thrice will carefully inching forwards
* 3-hit flurry into charge and forward special to kill the **BT**s
* Finish the remaining **Galsia**s while positioning far enough from **Altet** so he doesn't hit you on entry
* Forward special **Altet** when he lands to bounce him off the wall (i)
* Wait for the incoming **Francis**' to squat, then inch forward a bit
* When the **Francis**' land, jab him once, then immediately defensive special
* Jump forward, then blitz and jump kick, **Altet** and some **Francis**' may die
* Jump forward kick, then blitz into charge and jump kick to kill the remaining **Francis**'

Note:

(i) I made a mistake with my input with Altet here, don't follow the video example  

### 1.3 Freestyle

**BT**:

* Will usually quick jab you if he gets close enough
* Use longer ranged attacks/kicks to deal with him safely

**Francis**:

* Annoying because they're never in sync here
* Squats to telegraph he's about to start jumping
* Sometimes back off really far. Best to avoid this overall by killing him quick
* Can counter his jump with air special for better positioning (relative to forward special)

## 2. Before kitchen

### 2.1 Opening

* [Optional] Take the apple from the bin, then air dash right

### 2.2.1 Strategy - Street

* This section requires some setup for an easy chain into the kitchen
* Forward special into charge to kill the first **Donavan**
* 3-hit flurry into charge and forward special to kill **Altet**
* Ideally, the second Donavan comes to you after charge and gets hit by forward special (i)
* Hop to the left, grab the katana and throw it right
* Kick **Donavan** right, take the money, pick up the sai, then kick **Donavan** again
* Drop the sai, with the katana, and kill **Donavan** with charge attack
* Wall combo (FC) and jump kick the entering Galsias (or just use Charge attack)
* Take the chicken, then time a forward special on the entering **Raven**
* Air cancel **Raven** on his wake, then grab and throw him backwards
* Move to the edge of the shadow to prepare for the incoming **Condors**
* Wall combo (FC), jump kick to kill them
* Immediately air special right. Some freestyle is required now (*See 2.3 - Raven - Street*)

### 2.2.2 Strategy - Alley

* Kill **Condor** with the katana and sai
* Immediately jump forward kick then entering Raven before he lands. Kill him however
* Kill **Diamond** with the katana and sai
* Take the goodies in the cans, then move diagonally down to bait **Garnet**
* Time a forward special when she's about to jump to bounce her to you
* Air cancel her and finish her off
* Kill **Raven** with the stop sign and another weapon of choice
* Time throws of each weapon at the **Gourmand** blocking the door
* The weapons should land nicely where needed to extend into the kitchen
* Take the goodies in the cans, then dodge **Gourmand**'s jump (as necessary)
* 3-hit flurry into charge to bait the second **Gourmand**
* You'll want to use this guy to reposition closer to the weapons
* 3-hit flurry twice, grab forward hits (2x), release and re-grab him
* Grab forward 2 hit, blitz, then jump kick him on wake to finish
* Ideally, you would be perfectly in line with the weapons now

Notes:

(i) This didn't happen for me so I improvised. It seems to depend a bit on positioning forward enough that Donavan will aggro when your charge hits

### 2.3 Freestyle

**Raven - Street**:

* There are a few possible scenarios, listed from best to worst
* [Both jump] They both get caught in the air special. Follow up with blitz, jump kick to finish
* [1 jump, 1 block] Use the second hit of air special. Wait for the downed **Raven** to wake, then neutral special. This should hit the blocking one that now tries to kick you. Jump forward kick, air special, charge attack to kill him. Jump forward kick the second Raven, then kill him however
* [Both block] Air special left to position, then punch the closest **Raven**. If you're lucky the third hit will get him before he blocks again and you just need to neutral special to stay safe. Grabbing may work too
* [Other] Sometimes one jumps and the other backs off into the wall. Jump kick seems to solve this scenario but I'm not entirely sure

**Gourmand - Alley**

* If you're unlucky, he may come at you at a bad timing
* [Jump] Neutral special to stay safe, then try to air cancel him on wake to start a combo
* **Always** try to use him to position to the weapons where possible

## 3. Kitchen

### 3.1 Opening

* Throw each available weapon at **Ben** in the kitchen in a timed fashion to kill him
* You will need to jump forward 4 times into the kitchen in between throws

### 3.2 Strategy

* Position to kill **Altet** with 3-hit flurry, charge, forward special
* Follow up with a charge for safety against any incoming **Galsias**
* Forward special into charge, then forward special into neutral jump kick. This usually kills the huge crowd that's entering in a safe fashion. You may need to forward special one more time
* Stay close to the right wall, kill the incoming **Altets** anyhow you like
* Get the apple from the bin, then kick **Galsia** and any remaining enemies to the right wall
* Grab and blitz the last enemy, then jump kick to hold the combo
* Time a hit on the bin when the chicken is about to land, then rush and grab it
* Use wall combo (FC) into reverse neutral special on the entering enemies
* A bit of freestyle is usually required at this part

### 3.3 Freestyle

* The objective is to try and kill all but one enemy, then kick them to the right to extend
* One **Raven** will sometimes live (feels like 50-50), try to grab and kill him, but neutral special if needed
* Throw knives at the enemies to try and thin their numbers. You can also use air dash, but it may be unsafe if there are still knife **Galsias**
* Don't use the cleaver as it can accidentally kill all enemies (unless that's what you want)
* Kickboxers can be grabbed, released and immediately kicked. Use this if you need to position them
* Once you have one enemy against the right wall, grab them

## 4. After kitchen

### 4.1 Opening

* Blitz and jump kick into an air ride to extend to the next section

### 4.2.1 Strategy - Alley

* [Optional] Take the goodies as you like for this part
* A bit of a freestyling is needed for Dylan and Kevin (*See 4.3*)
* For the **Ravens**, open on them with air dash, as it has priority over their jump and normal attacks
* Kill them in any way preferred, using grabs, air cancels, throws, etc.

### 4.2.2 Strategy - Street

* Take the sai from the bin, then position and throw it at **Gourmand**
* Immediately air dash right. The next part depends on luck (i)
* If both **Bens** start blowing fire, continue reading, otherwise, freestyle is needed (*See 4.3*)
* Forward special to knock all 3 fatties down, then jump forward and inch forward
* Repeat the previous step once, then forward special once more
* Wait for the fatties to wake, then wall combo (FCF), jump kick, and air special
* At this point, they may all die, but you'll start the next section in a bad spot
* Ideally, one enemy might survive, and you can use him for positioning
* Try to kill the last enemy while you're near the fire can. This will make the next section easier

Note:

(i) I was unlucky and got the bad pattern in the video

### 4.3 Freestyle

**Dylans**/**Kevins**:

* For Dylan and Kevin, they like to avoid you when you try get closer
* To counter this, grab any that are close to you and release/grab them until the rest get closer
* Killing them is trivial, just do it anyway preferred

Street (alternate strategy):

* Okay so one **Ben** decided to walk. Start with a forward special
* Walk forward until one **Ben** rolls, then forward special
* Position to the apple box and forward special to push the incoming **Ben** away
* The next two fatties should come, forward special them 1-2 times to keep them near the barrel
* The timing to hit **Gourmand** while he's jumping is when he's close to hitting you, or it will miss
* All fatties should be dead now
* Open on the kickboxers with air special, then try to deal with them as best as you can
* Once again, try to kill the last enemy while positioned where the fire can was at

## 5. Goros

### 5.1 Opening

* Assuming you had a good positioning, the first **Goro** will almost immediately charge attack you, without the other interrupting

### 5.2 Strategy

* This part describes the ideal situation. Luck is often required
* Time a jump to dodge **Goro**'s attack, then flurry him with as many hits as you can get in
* The second **Goro** should be charging, dodge this with neutral special to get them in sync
* Jump forward twice to get behind them, wait for their block/charge animation to "lower"
* 3-hit flurry, then 4-hit flurry into neutral special to hook them to the right
* Follow up with blitz, double down attack, charge and forward special
* When they wake, jab and immediately neutral special. They can sometimes get out of sync here, so freestyle may be required.
* Assuming they stay in sync, follow up with blitz, double down attack, charge, neutral special and jump forward kick to the left. They should both be dead.
* Take the goodies and the knife, then proceed to the Dojo

### 5.3 Freestyle

* [Charge attack] Just time a jump then punish them
* [Charge attack] Alternatively, neutral special, jump behind them, and punish
* [Block] Jab and immediately neutral special, then follow up with blitz
* [Pincer attack] One of the worst scenarios. I don't have a good example of how to solve this unfortunately. You will probably need to neutral special and try get in a better position (with them both at one side), then counter-play them based on how they move

## 6. Galsias

### 6.1 Opening

* [Optional] Take the goodies in the vases
* Throw the knife at **Galsia** and jump forward to trigger the other **Galsias** to enter
* There are 30 Galsias in total
* You get 13 strikes per naginata, it's good to keep count of when it's going to break
* You'll want to break the naginatas to prevent Galsia from picking them up

### 6.2 Strategy

* Some luck required as its sometimes possible to get swarmed here
* Take the naginata and position at the center of the room
* Defend your position while trying to maintain a safe distance from the **Galsia**s
* If any of them get too close, throw the naginata and catch it, the throw animation is very fast compared to the swing
* After breaking 2 naginatas, the remaining **Galsias** is just freestyle.
* Take the third naginata once done and proceed to the next section

### 6.3 Freestyle

* If you accidentally grab Galsia from the front, use a forward combo to end him
* If you accidentally grab him from behind, suplex him
* Jump forward and use down attack if you need to get closer to grab and throw **Galsias** backwards. This is a good way to keep the crowd under control
* You can try use jump forward kicks left/right to stay safe, but there isn't a guarantee
* You can kick Galsias around to get in position to extend to the next room

## 7. Donavans

### 7.1 Opening

* For some reason I had less trouble with this room than with the **Galsias**
* The trick is to camp the right entrance, where it's easier to defend and kill them all
* Pay attention to your weapon durability as well, so it doesn't break mid combat
* Position and open by jumping forward, throwing the naginata, then kick

### 7.2 Strategy

* Kill **Altet** with 3-hit flurry, charge, forward special
* Take the katana and time a forward jump, backward kick on the waking Donavans, then throw and catch the katana to kill them
* Position slightly below the entrance, and be ready to kill entering or approaching enemies with freestyle
* Try to leave one enemy alive and kick him around to get the cash
* Lastly, try to get hold of a cleaver before you kill the last guy, then proceed

### 7.3 Freestyle

* [An enemy jumps in] Slash him with your weapon
* [Another enemy jumps in too fast] Neutral special, then forward special to stay safe
* [Enemies approach too slow] Throw your weapon or forward special, do **NOT** move
* [Weapon about to break] Throw it, pick up another and get back in position
* [Out of weapons] You can out of range any enemies without katanas with jabs, forward special otherwise
* When they're numbers are thinning, slow down and try to get a cleaver
* If the last enemy died before you got the cash, always take the cleaver before taking the cash, you might still be able to hop into the next room in time

## 8. Kickboxers

### 8.1 Opening

* It's likely you'll need to use a star in this room due to the RNG
* Position diagonally up and right a bit, jump, throw the cleaver (animation cancels), and air special

### 8.2 Strategy

* Depending on your luck, all the **Ravens** may die (i)
* Try to follow up on air special with charge and forward special for more damage on **Pheasant**
* The best case scenario is you get to safely follow up with a blitz, neutral special, jump forward attack left, air special, then a forward special on **Pheasant** to finish him
* That's really unlikely though, so freestyle normally needed again

Note:

(i) I encountered the worst case scenario in the video

### 8.3 Freestyle

* This room can drain your specials out really quickly, so you need to be fast
* The objective is to pressure the kickboxers hard enough and exploit i-frames to avoid hits
* Grabs and throws are one approach, but it isn't always safe when enemies are out of sync
* [Enemies in reach] Jab into neutral special. Neutral special move you back a bit, so you may need to also mix in reverse neutral special to maintain pressure
* [Enemies out of reach] Jump forward kick into air special/cycle to safety
* The jab and forward kicks are needed to replenish your drain a bit so you can special. Both the specials have pretty long i-frames, which can go through the split second frame that kickboxers lower their guard
* If you kill them all by accident, you can still chain to the next room by taking the apples, positioning in front of the door, then jump forward, and air dash. This is risky, thus the reason I didn't do it.

## 9. Random number god

### 9.1 Opening

* The extension here requires frame perfect timing...
* When the scene starts moving left, hammer your jump forward as fast as you can (I press it at least 5 times), then immediately air dash to land in front of Shiva

### 9.2 Strategy

* Open with 3-hit flurry, then 4-hit into neutral special to hook him to the left
* From here on it depends a bit on luck (there is a non-luck dependent way, but I wasn't able to execute it, this is described in freestyle)
* Best case is that Shiva tries to palm strike, you dodge, and punish him into an infinite
* If you drop the infinite, chances are the combo will bank or be lost and you'll have to freestyle
* That's it, you've beaten the random number god

### 9.3 Freestyle

**Shiva**:

* These are his moves, and how to counter them
* [Rapidly walking to you] Wants to punch, just out of range him with your punches
* [Palm strike] Side step, then punish
* [Full moon kick in place] Usually on wake if you're next to him, counter with neutral special
* [Double full moon kicks] Just avoid, then punish
* [Random jumps] Usually does this 2-3 times, then follows up with an attack
* [Shadow clones] Don't have a good solution to this, just need to avoid them

## Theoretical maximum score

Estimated: **157k+**

Lost points:

* Pickups: 4.2k
* Combo bonus: Variable (i)
* Health: 990
* Stars: 1k
* Time: Variable

Notes:

(i) Kickboxers in the Dojo spawn infinitely as long as Pheasant is alive, so points can be farmed there, but it's **VERY** risky
