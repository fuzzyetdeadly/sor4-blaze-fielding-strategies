# Stage 07: Skytrain

[Return to index](./tipsandtricks.md)

[![Blaze Stage 7](./../assets/images/Splash_S07_Score.png)](https://www.youtube.com/watch?v=RnrWSmTTQXw)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

One of the shorter and less stressful stages to figure out. The station and boss fights are a little tricky, while the rest of the level can be played with pretty good consistency.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start (Good)](#1-start)
2. [Station (Good)](#2-station)
3. [Taser-face! (Good)](#3-dicks-and-barneys)
4. [Boss fight (Average)](#4-estel-and-commissioners)

## 1. Start

### 1.1 Opening

* Jump forward while avoiding the train hazard until you reach the first **Ferrocio**

### 1.2 Strategy

* 3-hit flurry, charge, forward special to open. This will push the cops back a bit
* Forward jump kick once to push them back further
* 3-hit flurry, forward special. This will sometimes finish all of them
* Hold charge, release it on the first **Dick**, then forward special
* Reposition in front of the next two **Dicks**, and time a jump kick right after the 3rd warning bell.
* Follow up with an air special. This will sometimes kill all enemies on screen
* Three more cops will enter. Kill them with forward special, charge, jump kick
* The last two cops will enter. Kill them with forward special, jump kick, air special. The air special is critical, or you won't be able to on-the-ground (OTG) the cops afterwards

### 1.3 Freestyle

* The general idea is to pressure the mobs to the right wall, if you want to hold the combo chain
* If things don't go as planned, use forward jump kick and throws to get the mobs to the right
* You'll need to pay attention to the train hazard, as it can prematurely kill the mobs, causing the combo to drop
* Any train hazard will immediately disappear when the last cop dies

## 2. Station

### 2.1 Opening

* Blitz the freshly killed cops, jump down attack x2, jump down attack x2 (situational), charge, back attack to hold the combo

### 2.2 Strategy

* 3-hit flurry, charge **Altet** to push him away. I don't kill him straight to prevent the cops from entering
* 3-hit flurry, charge, forward special whichever **BT** or **G. Signal** is running towards you Neutral special if signal decides to slide (forward special sometimes misses)
* *Situational*: You can start fighting any remaining signals or the entering blue cops. Kill them any way you like, but watch out for any trying to hit you from behind
* *Situational*: **Donavans** and **Altets** will enter. Some improvisation is usually needed here to hold it together. In the video reference, I use air cycles a bit to avoid getting uppercut
* **Francis** will enter. You need to pre-empt this and position to grab the left most one, and throw him right. Kill the ones you can with forward special into charge (or neutral special)
* The remaining should have also entered. Kill Barney first in any way you like; I was lucky in my recording as he got caught in the combo I did on **Francis**
* Finish off the remaining cops, and the combo will bank before you can proceed.

### 2.3 Freestyle

* For the situational sections, you will probably need to use your specials to stay safe
* If you can prevent the cops from fighting the bad guys, it's a bonus to your score. 2318 damage is the most that is possible for the first part of the full combo

## 3. Dicks and Barneys

### 3.1 Opening

* Try to approach from roughly the vertical center of the train. Jump forward a few times

### 3.2 Strategy

* Kill the first two **Dicks** with 3-hit flurry, charge, forward special
* Move diagonally down and forward a bit, and immediately forward special to push **Dick** and **Barney** back
* From here on, I kind of freestyle the rest of this section

### 3.3 Freestyle

* The objective is to try and get all the enemies up against the right wall, then jump kick them to death. After you land the last hit, immediately air special to position yourself closer to **Estel**
* This can be achieved in the same fashion as before, with forward jump kicks, throws
* Watch out for Tasers and the train hazards, it's faster this time

## 4. Estel and Commissioners

### 4.1 Opening

* Best way to open is to air special the last **Barney** after killing him against the wall
* Second best way is to throw an item at **Estel**. She will almost always open with a grenade if you are far away

### 4.2 Strategy

* I don't have a concrete strategy for this fight, but these are some guidelines on what to try and do
* If you're near **Estel**, walk towards her diagonally if you're near, or she might punch you
* 3-hit flurry her twice, then charge attack and optionally forward special. This should bounce her off the wall behind you
* If she lands on her feet, immediately 3-hit flurry her twice again, or use whatever combo you like
* If she doesn't, be prepared to neutral special to dodge her neutral special, then use whatever combo you like
* For both the previous scenarios, try to finish by pushing her to the left so you can take the items
* Break the star box and take it, then dodge the incoming **Estel** and go for the food
* She will usually call for backup around this time, then some freestyle is needed

### 4.3 Freestyle

General:

* Best case situation is you manage to get all enemies in sync
* If they're in sync and also against the wall, it's possible to time a forward special, blitz loop to kill them starless (but this is a bit risky)
* Getting them in sync is tough, but using air specials and throws might help you achieve it. Otherwise, it might just depend on your positioning
* Killing the Commissioners isn't necessary, it's just for extra points. You can always try to "touch of death" Estel instead

**Estel**:

* *Flurry*: Counter with forward special or air specials. If safe, follow up with blitz
* *Flying kick*: Counter with forward special or air specials. If safe, follow up with blitz
* *Grenade*: Air dash away from the bomb site. If it's safe, you can also pick it up and throw it back
* *Wake attack*: Just use neutral special, then punish

**Commissioners**:

* *Grab attack*: Counter with forward special or air specials. Alternatively, step backward a bit, and punish on arrival
* *Red attack*: Counter with forward special or air specials. Alternatively, just punch him when he's near enough
* If you're next to him on wake, just punch him during his startup animations, and counterplay Estel if she gets too close

## Theoretical maximum score

Estimated: **64k+**

Lost points:

* Pickups: 1k
* Combo bonus: 1k+ (from lost damage)
* Health: 820
* Stars: 1k
* Time: Variable
