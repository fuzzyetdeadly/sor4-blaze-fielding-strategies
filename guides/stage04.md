# Stage 04: Old Pier

[Return to index](./tipsandtricks.md)

[![Blaze Stage 4](./../assets/images/Splash_S04_Score.png)](https://www.youtube.com/watch?v=DG--WkYBoaU)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

This stage is a huge gateway level for anyone new to the game, but once you're familiar with the enemy spawn patterns, it gets a lot easier to kill them before they become a threat. I borrowed some speed-running strategies for this one, as I was having some trouble getting certain bits consistent.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Start pier (Good)](#1-start-pier)
2. [Mid pier (Good)](#2-mid-pier)
3. [End pier (Average)](#3-end-pier)
4. [Beach (Average)](#4-beach)
5. [Estel (Average)](#5-boss)

## 1. Start pier

### 1.1 Opening

* Immediately move up and position in line with the barrel
* Jump towards the barrel twice, kicking it during the second jump
* Immediately air dash left, wait for the **Diamonds** to jump in, then jump left, kick right and air dash right. This usually causes them to fall into the pit

### 1.2 Strategy

* Walk towards the **Ruby** while she's jumping at you, and she should go right over your head. Grab her and throw, then hold charge
* Inch downward a bit, and release the charge into forward special when she wakes
* If you're lucky this can instantly kill the entering Ruby and also the BTs. Otherwise, just finish them off as convenient
* Take the apple, then move vertically downwards. Jump and air special to kill the **Diamonds** attempting to jump you
* Move left, and double air special to knock the **Rubies** down. Follow up with a blitz and jump double down attack, then air cycle left and right to also hit the incoming BT. If all went well, the **Rubies** should be dead, and the next two enter
* Approach and grab the next two Rubies and throw them back into the pit. This can be timed to also knock other approaching enemies in
* Kill all remaining enemies as convenient, while advancing to the right

### 1.3 Freestyle

* You can counter the girls jump kick with a timed neutral jump kick or air dash
* BT likes to sneak up behind, so be ready to defensive special just in case
* Sometimes enemies won't die as you expect them to, in which case, try to use attacks/throws that will land them in the pits

## 2. Mid pier

### 2.1 Opening

* Break the two cans and hold charge, then release it on the two Donavans at the right. They should die instantly

### 2.2 Strategy

* Move left and forward special into charge, then neutral special and jump kick right. If an enemy at the left survives, grab them and throw them right
* Two waves of enemies will jump in. I like to pick up the money bag to buy time while waiting for the second wave, then forward special once to knock them all off
* For the final wave, position and jump kick right to kill a Donavan and G. Signal, leaving one alive
* [*Optional*] Use the last G. Signal to leech your life back and get the turkey points as possible
* Advance right, take the suitcase, then move down and time a neutral special to kill all the jumping girls. After that, you can extend right with a forward special, then air dash across the gap
* Take the star and suitcase, then move down and forward special to kill the entering **Donavans**
* Move up and forward special left, then move diagonally left and down, and forward special right. All **Donovans** should be dead now
* Kill the entering Galsia and take his knife. Throw it to kill another
* Move up a bit, and forward special the 3rd **Galsia**, then position downwards. Grab the 4th **Galsia** and wait for the 3rd to get close. Forward special to kill them both
* Grab the **Diamond** that jumps in, count to 3, then throw her backwards. This should kill **Signal** and the entering **Rubies**
* Move up a bit and forward special left to knock one of the entering Diamonds into the hole. Jump in and down attack to catch the last one, then vault over her and blitz, jump kick, blitz, charge to leech back what life you can. Take a knife for the next part

### 2.3 Freestyle

* If you can't comfortably use charge attacks, you can also forward special, then defensive special towards the wall. This will hook enemies behind, making it easier to push them off the pier
* Sometimes the timing against the waves will be off, in which case it's safer to just use forward special to push them off
* Sometimes more jumping girls than ideal survive. If you're low on special, remember to use neutral jump kick to counter them instead
* Sometimes, G. Signal might grab you immediately after you throw Diamond backwards. It's just tough luck when this happens

## 3. End pier

### 3.1 Opening

* Try to position, jump forward, throw the knife and immediately air dash. This can put you in a good position to easily combo the cops
* If you don't plan to enter the arcade, a wall combo (SCS), jump kick, air dash, charge, reverse defensive special should kill the cops. Skip the arcade extension strategy in the next part
* Otherwise, you'll want to forward special to bounce them left, then jump right and forward special them once more to get them fighting with the mobs

### 3.2 Strategy

Arcade extension:

* Take the taser and enter the arcade. The trick to **Zamza** is to catch him before his AI activates. To do so, jump forward twice, with a down attack on the second jump
* Hit him up to 5 times, then grab and forward hit him twice, vault over and blitz
* Follow up the blitz with forward jump kick, air special, charge, forward special
* Then jump kick, air special, charge, reverse defensive special, and finally blitz
* Position downwards but slightly diagonal to **Zamza**, as this has a better chance he tries to come from the front, where you can grab, vault, blitz, and jump quick twice to break the box and extend out of the arcade machine
* Leave the arcade and continue

Dealing with mobs:
* This part requires freestyle. You'll want to try keep all the mobs to your left, so you can bounce them against the wall
* A few wall combos (SC or CS) should wipe them out pretty quick
* Once they're all dead, take the food and air dash into the pit

### 3.3 Freestyle

* Jumping girls are annoying to deal with, as they can jump over your forward special. Be careful around them. neutral jump kick is safe here as you don't want to get surrounded
* Best way to deal with **G. Signal** is to bait him with your back to him, then jump down attack and punish when he's committed. If the **Donovans** get too close, you'll need to forward special to stay safe

## 4. Beach

### 4.1 Opening

* Approach **Galsia** and forward jump kick him. Hold charge attack, move down and immediately release it on **Y. Signal** with a forward special follow up. This can kill 1-2 of the enemies 

### 4.2 Strategy

* First part trivial and can be freestyled. Wipe them out as you like
* The second part with the Signals and **Galsias** is tricky. It also requires freestyle (see below)

### 4.3 Freestyle

* First section of the beach is straightforward. Just watch out for **G. Signals** trying to slide from behind, and neutral special to stay safe where necessary
* Try not to kill the last enemy near the right wall, as the Signals in the next area can suddenly slide into you
* For the second section, the objective is to push all G. Signals right, and pin them against the wall. If you get surrounded, try to position somewhere that you can star and kill them all at once
* Use jump kicks to stay safe from knife Galsia as necessary
* Don't forget to also throw enemies to kill enemies that are out of range!
* [*Optional*] To extend to the boss fight, you'll need to save the apple for when all enemies are dead. Take a broken bottle, time the pickup, then move towards the rope. Throw the bottle and jump forward, it should hit the can and extend the fight

## 5. Boss

### 5.1 Opening

* [*Optional*] To get the turkey points, move diagonally up and right. Estel will eventually make her move, allowing you to punish her from behind with a 3-hit flurry into charge
* Try not to hurt her too much if aiming for the turkey, or backup will come

### 5.2 Strategy

* This fight is sort of a freestyle one. Ideally, you'll want to manipulate Estel into not calling for backup. This seems to depend on the direction she's facing when she wakes from a knockdown
* Try to position or get Estel near a wall. It's a lot easier to combo her against one

### 5.3 Freestyle

**Estel**:

* I was actually trying to touch of death Estel in the video, but it wasn't working right so I improvised with manipulation
* My preference is to use the turkey or money to hold the combo, while waiting for her to approach. Once she telegraphs her attack, I evade, then punish her
* In the video, I open with some auto-combo, then a grab and back throw. This is followed by a jump down attack, air special, and back attack
* For the touch of death, moving torwards her and doing a back-attack is supposed to pick her up and allow a wall combo, but I couldn't get it working, so I follow up with blitz, double down, charge, reverse neutral special, and two punches before she lands (makes her face you)
* Wait for her to almost wake, then neutral special. She should try to punch you and whiff, then you just need to repeat the previous two points until she's dead. I had to do this 3 times to finish her off, and that's it!

Contingency:

* If Estel faces away from you while she wakes (when forgetting to punch twice), she will usually try to do her spinning kick, and immediately call backup. If you still have stars, you should use them at this time to try avoid that
* [*Backup requested #1*] The bazookas always have two hits. One at the center of the circle, then a ring of fire. You can safely stand on the ring while it's hitting the center. Knowing this helped me conserve my specials a bit
* [*Backup requested #2*] Can either hit cops and forward special when Estel gets too close, or use a star to clear the ground. NEVER use a star when you don't see an incoming bazooka, as it can cause you to get hit if it comes during your star
* [*Backup requested #3*] Three bazookas in quick succession. This sucks because it forces you to weave, often meaning you'll lose the combo. Forward special Estel if she gets too close. Alternatively, you can air dash her as well (it has I-frames)

## Theoretical maximum score

Estimated: **109k+**

Lost points:

* Pickups: 1.3k
* Combo bonus: 3.4k+ (Backup cops 54 x 4 damage)
* Health: 230
* Stars: 500
* Time: Variable
