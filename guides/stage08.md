# Stage 08: Art Gallery

[Return to index](./tipsandtricks.md)

[![Blaze Stage 8](./../assets/images/Splash_S08_Score.png)](https://www.youtube.com/watch?v=xEyA_NLtXJM)

![More info](./../assets/icons/icon_info.png) *Image contains link to full-combo reference on YouTube*

This stage felt as if it had the worst RNG in the entire game. The grenade girls and bottle bitches make it very challenging to hold the combo together due to their constant moving around. Very often I also found combos dropping due to getting stuck in poison pools. There is a general strategy to it, but it requires a lot of freestyle, and has a generally poor consistency.

## Terminology

Some jargon terms are used in this topic. If you happen across any you don't recognize, please refer to [**techniques**](./../extras/techniques.md) for a summary of the said terms.

## Sections

Use these to jump to specific sections

1. [Entrance (Good)](#1-entrance)
2. [Chicken exhibit (Good)](#2-chicken-exhibit)
3. [Cooler area (Bad)](#3-cooler-area)
4. [Grenade fight (Average)](#4-grenade-fight) - Cashier area excluded, it's rather trivial
5. [Boss fight (Average)](#5-snake-twins)

## 1. Entrance

### 1.1 Opening

* N/A

### 1.2 Strategy

* Forward jump, air dash, then immediately forward special. This will kill the first **Victoria**
* Grab the fire bottle, jump forward, align and throw it. This usually kills 3 **Victorias**
* Immediately jump backward, then position and back attack the **Victorias** one after another

### 1.3 Freestyle

* Sometimes one **Victoria** might survive at the right. Simply jump over and kill her however you like

## 2. Chicken exhibit

### 2.1 Opening

* Jump over from the previous section, forward special into charge **Donavan** and **Galsia** to kill the former

### 2.2 Strategy

* Try to keep the Golden Chicken pedestal in place. It obstructs the AI's pathing
* Try not to get too far from the left wall, so you can kill **Victorias** on their entry
* Use forward special or any combo of your choice to keep **Donavans** and **Lous** to the right
* Enemies walking slowly in a semicircle are usually trying to flank you from behind, don't let them
* Be warned, it was possible to get a good consistency for this, but it's also very easy to muck it up

### 2.3 Freestyle

**Victorias**:

* Back attack when they're holding a bottle to immediately kill them
* Can also forward jump kick them if they're near wall to kill them if they have a bottle
* Vulnerable during her projectile throw animation
* If she escapes, try to hold the combo by hitting other things, and target her when she's vulnerable
* If an enemy approaching fast from the front, neutral special, jump forward kick, air special, charge to kill her

**Lous**:

* *Grab*: Can be countered with a jump down attack, then punish
* *Grab*: Alternatively, just forward special to stop them from groping you
* Take advantage of them by kicking them around to hold the combo together

**Donavans**:

* Try to kill them fast to avoid accidental jabs or uppercuts
* Also easy to counter with forward specials

## 3. Cooler area

### 3.1 Opening

* Assuming the Golden chicken was in a convenient location, throw it at **Galsia** to hold the combo
* Approach **Jonathan**, but not too close to avoid triggering enemies further right
* Kill him with two 3-hit flurries, grab combo and charge

### 3.2 Strategy

* Don't kill the enemies too fast at the start, or you're gonna have a bad time
* Try to avoid standing on poison pools (very hard), they are serious combo breakers
* Align with the wall and jump in to kick **Elizabeth**
* Go for the apple at the top left, then move diagonally down and right
* Jump right and kick the **Lous** left
* Go for the chicken in the cooler, then move diagonally down and right until the Lou at the right tries to grab you
* Jump right and kick the **Lou**, then position to kick the **Elizabeth** at the right. There is a chance this might cause her to die before she becomes a threat
* Use jump kicks and throws to make your way back to the entrance at the left. As long as you pass the cooler, all the **Condors** should spawn there after two of the pre-spawned enemies in the cooler area die
* Forward special, jump kick, air special the **Condors**, which can kill most of them
* Neutral special 3-4 times until there are a nice amount of enemies near you, then star and forward special right or left to kill any survivors
* It's freestyle from here, try to kill all enemies but one, and use the last to chain to the next area

### 3.3 Freestyle

General:

* You can grab and release enemies to re-position them up and down the Y-axis. This will sometimes only work a few times before they break free, release the grab and forward jump and kick them to prevent this

**Kickboxers**:

* *Jump attack*: Counter with neutral special or air dash
* *Shin kicks*: Telegraph is fast approach toward you (hard to see with poison pools). You can out of range them or forward special to counter this, or air dash if you're in a bad spot
* If you can grab them, don't kill them immediately, try to throw them into **Elizabeth**

**Elizabeth**:

* *Escaping poison pools*: Blitz into air special or grab an enemy if you can, and throw your way out of it
* *On wake*: Use air cancel to avoid her shove, then grab her
* Vulnerable during her projectile throw animation
* Is dumb and will often die to her own poison, causing combo breaks
* Likes to move up and down and jump away from you when you try to approach
* Will sometimes jump behind you and immediately shove
* Try to catch by throwing other mobs into her, then closing the gap and catching her
* If you can, catch and use her to extend the combo to the next area. This can be done with a combination of grab and release and jump forward kicks if she has enough life
* If she doesn't, you'll need to "air ride" her (opening with blitz, forward jump kick) 3 times near the bottom of the screen to chain to the next section

## 4. Grenade fight

### 4.1 Opening

* At the hallway, kick the **Lous** to the right wall
* Kill them with blitz, jump kick, double air special
* Inch forward, forward special **Margeret**, and immediately forward jump kick her

### 4.2 Strategy

* Throw the first Margeret's grenade left, and pray it kills at least 2 others (will trigger **Lous** to spawn)
* Immediately air cancel the waking **Margeret** and grab her. Hold and wait until your grenade pops, then throw her backwards
* Beeline for the grenade at the center, time a forward special on the 3 incoming **Lous** while holding it
* Jump right and kick the two more incoming **Lous**, then immediately pick up any stray red grenades and throw them left and right. This should kill the 3 **Lous** from the left (and maybe more)
* Kill the **Lous** at the right and try to only finish when the entering **Elizabeths** have finished throwing their bottles. This ensures you don't have a poison pool in the middle of the room
* Kill the **Elizabeths** with a wall combo (SC) into jump kick
* Watch for any red grenades and forward kick the **Victoria** entering from bottom right to kill her
* Immediately jump left and forward kick the **Victoria** entering from the bottom right
* Go for the next Victoria at the left and kill her, then immediately make way back right to wall combo **Elizabeth** and any others

### 4.3 Freestyle

General:

* Always aim for **Victorias** and **Elizabeths** first, they're the highest threat.
* If you don't kill them, extra **Margerets** will come in (up to 7 in sets of 2 + 2 + 2 + 1 if I'm not mistaken)
* There will also be annoying pools of goo that will impact your mobility
* You will sometimes need to grab and hold enemies before throwing them to hold the combo together
* All the girls are vulnerable when they're trying to throw, this is the best time to aim for them so they don't jump away

## 5. Snake twins

### 5.1 Opening

* *Optional*: Don't take all the pickups, leave two bags of cash as insurance to extend the combo
* Carry the taser into battle, you might get a lucky shock

### 5.2 Strategy

* Beeline diagonally right and up and throw the taser. Sometimes it'll hit a twin, other times a box
* Take both the food pickups, then it's all freestyle from here

### 5.3 Freestyle

General:

* Try to kill **Riha** first, because if **Beyo** dies you'll have nothing to hit during fire pools
* The twins will often try to attack you from both sides, so you need to blitz into air special out if it gets too dangerous
* The objective is to try get the twins in sync to instantly kill them both at once
* There are a few ways to do this, but a lot of luck is involved, and it's very situational
* I will try to describe some scenarios to get them in sync below

**Riha (Fire)**:

* *Fire snake*: Dodge up or down, jump towards her and punish
* *Fire pool*: Will usually follow up with Fire snake. You can safely forward special 2-3 times before you need to dodge. Do this only if it's safe

**Beyo (Poison)**:

* *Snake attack*: Jump in and down attack her, then combo her
* *Poison pool*: Just keep beating her as long as it's safe
* Use blitz into air special to escape from her pool if you need to

Scenarios to sync:

1. While hitting **Riha** from the **front**, and **Beyo** tries to Snake attack from **behind**:  
    Grab **Riha** and throw her into **Beyo** against the wall
2. While hitting **Riha** from the **front**, and **Beyo** tries to Snake attack from **front**:  
    Grab **Riha** and blitz into double air special
3. While hitting **Beyo** from the **front**, and **Riha** tries to Snake attack from **behind**:  
    Grab **Beyo** and time a throw of her into **Riha** into the wall. If you time it right, you should avoid **Riha**'s fire and also knock her down
4. While hitting **Beyo** from the **front**, and **Riha** tries to Snake attack from **front**:  
    Grab **Beyo** and blitz into double air special

* For cases 2 and 4, you may not have time to grab, so you need to immediately blitz to avoid damage
* For all these scenarios, you can follow up with a blitz on the ground (OTG), 1-3 jump down attacks (depending on how well they synced), and an air cycle.
* The left wall seems to be more reliable for jump down attacks, at the right, sometimes one of them might drop
* After that, I usually wait for them to wake half way (gives you an additional OTG hit), star power, wall combo (SSC) them, and finish them off
* There are a lot of other scenarios, such as hitting either twin from the back or front, it's quite difficult to describe them all, practice is the best way to figure out this fight
* ... or you can just infnite them to death :)

## Theoretical maximum score

Estimated: **102k+**

Lost points:

* Pickups: 2.2k
* Combo bonus: 7k+ (from lost damage)
* Health: 140
* Stars: 1k
* Time: Variable
